#!/bin/bash
export ROS_MASTER_URI=http://localhost:11311
set -euxo pipefail
OEX=/opt/ouster/shared_sw/ouster_example
bagdir=/home/dllu/bag/purplepup
#bag=$bagdir/os2_sample_0.bag
#meta=$bagdir/os-992009001226.local-os2.json
bag=$bagdir/os2_128_drive_042920_*.bag
meta=$bagdir/os-992017000218.local_1024x10.json
#bag=$bagdir/os1_sample.bag
#meta=$bagdir/os-992011000781.local-os1.json
#bag=$bagdir/os0-128-warehouse-sample-data.bag
#meta=$bagdir/os0-128-sample-config-ROS.json
#bagdir=/home/dllu/bag/os0
#bag=$bagdir/beta3_03-09_os0new_4.bag
#meta=$bagdir/os-992009000995.local.json
pushd $OEX
CMAKE_PREFIX_PATH=$OEX

pushd ouster_client
mkdir -p build
pushd build
cmake ..
make -j12
popd
popd

pushd ouster_viz
rm -r build
mkdir -p build    
pushd build
export CMAKE_PREFIX_PATH=$OEX
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j12
popd
popd

popd

cd ~/
mkdir -p ros_ws

source /opt/ros/*/setup.bash
pushd ros_ws
mkdir -p src
ln -sfT $OEX src/ouster_example
catkin_make_isolated -DCMAKE_BUILD_TYPE=Release
popd

source ros_ws/devel_isolated/setup.bash
sleep 5
roslaunch ouster_ros ouster.launch replay:=true metadata:="$meta" viz:=true image:=true &
sleep 5
rosbag play -r 2 --loop $bag /os1_node/lidar_packets:=/os_node/lidar_packets /os1_node/imu_packets:=/os_node/imu_packets 2> /dev/null > /dev/null

