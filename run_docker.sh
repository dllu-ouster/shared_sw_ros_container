#!/bin/bash

# set XAUTH things for graphical applications
XAUTH=/tmp/.docker.xauth
if [ ! -f $XAUTH ]
then
    xauth_list=$(xauth nlist :0 | sed -e 's/^..../ffff/')
    if [ ! -z "$xauth_list" ]
    then
        echo $xauth_list | xauth -f $XAUTH nmerge -
    else
        touch $XAUTH
    fi
    chmod a+r $XAUTH
fi

#cp build_shared_sw.sh /home/dllu/warden-dev-ws/

xhost +local:root
docker run -it \
    --device=/dev/dri \
    --network=host \
    --env="DISPLAY=$DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    -v="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --env="XAUTHORITY=$XAUTH" \
    -v="$XAUTH:$XAUTH" \
    -v="/home/dllu/ouster:/opt/ouster" \
    -v="/home/dllu/warden-dev-ws/:/home/dllu/:rw" \
    -u dllu:dllu \
    pupros \
    bash
xhost -local:root
