#!/bin/bash
set -euxo pipefail
docker build \
    --build-arg DEVEL_UID=$(id -u) \
    --build-arg DEVEL_GID=$(id -g) \
    -t pupros .
