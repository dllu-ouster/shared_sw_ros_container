FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ARG DEVEL_UID=65534
ARG DEVEL_GID=65534

RUN set -xu \
# add some packages
&& apt-get update \
&& apt-get install -y adduser apt-utils bzip2 debconf gnupg iproute2 git \
        iputils-ping kmod less locales mawk net-tools netcat-openbsd passwd \
        procps sudo tzdata ubuntu-keyring vim-tiny whiptail ncurses-term tmux socat ccache\
# set up dllu user
&& groupadd -o -g ${DEVEL_GID} dllu \
&& useradd -o -m -s /bin/bash -u ${DEVEL_UID} -g ${DEVEL_GID} dllu \
&& echo "dllu ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
# set locale
&& /usr/sbin/locale-gen "en_US.UTF-8" \
&& sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
&& dpkg-reconfigure --frontend=noninteractive locales \
&& update-locale LANG=en_US.UTF-8

ENV LANG="en_US.UTF-8" \
    LC_ALL="en_US.UTF-8" \
    LANGUAGE="en_US.UTF-8"

LABEL "warden-dev"="" "WORKSPACE"="${HOST_WS}"

RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu focal main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

RUN apt-get update && \
    apt-get install -y --no-install-recommends -f && \
    apt-get install -y --no-install-recommends \
    sudo \
    tmux \
    libtclap-dev \
    libglfw3-dev \
    libglew-dev \
    build-essential \
    ros-noetic-desktop-full \
    screenfetch \
    mesa-utils \
    software-properties-common \
    vim

RUN apt-get update && apt-get dist-upgrade -y

# CMD /usr/bin/tmux new-session -d && tail --pid=$(pgrep tmux) -f /dev/null
CMD trap : TERM INT; sleep infinity & wait
